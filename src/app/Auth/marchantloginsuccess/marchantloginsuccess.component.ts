import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-marchantloginsuccess',
  templateUrl: './marchantloginsuccess.component.html',
  styleUrls: ['./marchantloginsuccess.component.css']
})
export class MarchantloginsuccessComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit() {
  }
  isLoading: boolean=false;
  dynamicText;

  AgreeAndConfirm() {
    this.isLoading = true;
    this.dynamicText = "Your account verification has been successfully setup.";
    setTimeout(() => {
      this.dynamicText = "Please wait while the cart payment is being processed.";
      setTimeout(() => {
        this.isLoading = false;
        this.router.navigate(['/mywallet']);
      }, 2000
      );
    }, 2000
    );
  }
}
