import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../../../Shared/SharedServices/CommonService';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../Services/User.Services';

@Component({
  selector: 'app-merchantlogin',
  templateUrl: './merchantlogin.component.html',
  styleUrls: ['./merchantlogin.component.css'],
  providers: [CommonService, UserService]
})
export class MerchantloginComponent implements  OnInit {

  loginModel: any = {
    Email: '',
    Password: ''
  }
  isLoading: boolean=false;
  constructor(private route: Router, private commonService: CommonService, private ToastrService: ToastrService, private userService: UserService) { }

  ngOnInit() {
    $(document).ready(function () {
      $(".button").click(function () {
     alert(1)
            });
    });

  }

  loginSubmit(f: NgForm) {
    if (f.valid) {
      let UserExist: any = this.commonService.GetUserList().filter(i => i.UserName.toLowerCase() == this.loginModel.Email.toLowerCase() && i.Password == this.loginModel.Password);
      if (UserExist.length > 0) {
        this.isLoading = true;
        setTimeout((router: Router) => {
          this.isLoading = false;
          this.ToastrService.success("Login Successfully", "Login")
          //this.userService.SaveUser(UserExist);
          this.route.navigate(['/marchantloginsuccess'])
        }, 3000);
      }
      else {
        this.ToastrService.error("UserName or password is incorrect", "Login Error")

      }
    }
  }

}
