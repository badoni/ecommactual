import { Component, OnInit, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { NgForm, FormGroup, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BuiltinFunctionCall } from '@angular/compiler/src/compiler_util/expression_converter';
import { Router } from '@angular/router';
import { UserService } from '../../Services/User.Services';
import { Modal, ModalModule } from 'ngx-modal';
import { EmiDetail } from './EmiDetail';
import { IMyDpOptions } from 'mydatepicker';
import { isNullOrUndefined } from 'util';


@Component({
    selector: 'app-bonuspage',
    templateUrl: './bonuspage.component.html',
    styleUrls: ['./bonuspage.component.css'],
    providers: [UserService]
})
export class BonuspageComponent implements OnInit, AfterViewInit {
    private myDatePickerOptions: IMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    // Initialized to specific date (09.10.2018).
    // public model: any = { date: { year: 2018, month: 10, day: 9 } };
    public EmiModel: EmiDetail
    public pieChartLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail Sales', 'test1', 'test2'];
    public pieChartData: number[] = [300, 500, 100, 200, 100];
    public pieChartType: string = 'pie';

    @ViewChild('verifyDetail') someInput: ModalModule;
    panidentity = {
        'PanNumber': '',
        'AadharNumber': '',
        'otp': '',
    }
    DebitdetailModel = {
        'DebitCardNumber': '',
        'DebitcardHolderName': '',
        'ExpiryMonth': '',
        'Expiryyear': '',
        'CVV': ''
    }
    DebitaggrementdetailModel = {
        'DebitLoanAmount': '',
        'DebitEMi': '',
        'DebitIntrest': '',
        'DebitTenure': '',
        'isTermsconditionchecked': false

    }
    public myForm: FormGroup;
    paymentMode: string;
    islevel1notprocessing = false
    islevel1processing = false
    islevel1processed = false
    islevel2notprocessing = false
    islevel2processing = false
    islevel2processed = false
    islevel3notprocessing = false
    islevel3processing = false
    islevel3processed = false
    isRequestLoading = false;
    dynamicText;

    showENACHForm: boolean = false;
    showConfirmation: boolean = false;
    showOTPVerification: boolean = false;
    showEsign: boolean = false;
    showStepWizard: boolean = true;
    showTermsConditions: boolean = false;
    showProcessing: boolean = false;
    isShowverification: boolean = false;
    isLoading: boolean;
    shownstep2: boolean = false
    shownstep3: boolean = false
    shownstep1: boolean = false
    step1class = ''
    step2class = '';
    step3class = '';
    isAgreementChecked: boolean;
    inactiveclass: string = 'btn btn-circle btn-default';
    activeclass: string = 'btn btn-circle btn-default btn-primary';
    isOfferChecked: boolean = false;
    ShowDebitCardDetail: boolean = false;
    isloadingredirect: boolean = false;
    showbankdetail: boolean;
    SliderStartingIndex: any;
    constructor(private ToastrService: ToastrService, private fb: FormBuilder, private myRoute: Router, private userservice: UserService) {

        this.EmiModel = new EmiDetail();
        this.myForm = this.fb.group({
            fromDate: '',
        });
    }
    setDate(): void {
        // Set today date using the patchValue function
        let date = new Date();
        this.myForm.patchValue({
            myDate: {
                date: {
                    year: date.getFullYear(),
                    month: date.getMonth() + 1,
                    day: date.getDate()
                }
            }
        });
    }

    clearDate(): void {
        // Clear the date using the patchValue function
        this.myForm.patchValue({ myDate: null });
    }

    ngOnInit() {
        this.shownstep1 = true;
        this.step1class = this.activeclass;
        this.step2class = this.inactiveclass;
        this.step3class = this.inactiveclass;
        this.setDate();
    }
    ngAfterViewInit() {

    }
    sliderUpdate(evt) {
        debugger;
        if (evt.from) {
            this.SliderStartingIndex = evt.from
            this.EmiModel.Timeduration = evt.from
        }
    }
    sliderChange(evt) {
        debugger;
        if (evt.from) {
            this.SliderStartingIndex = evt.from
            this.EmiModel.Timeduration = evt.from
        }
        this.CalculateEMI();
    }
    sliderFinish(evt) {
        debugger;
        if (evt.from) {
            this.SliderStartingIndex = evt.from
            this.EmiModel.Timeduration = evt.from
        }

    }
    PanDetailSubmit(f: NgForm, data: any) {
        debugger;
        if (f.valid) {
            if (this.isAgreementChecked) {
                this.isLoading = true;
                setTimeout(() => {
                    this.isLoading = false;
                    this.isShowverification = true;
                    if (this.panidentity.otp) {
                        this.ClearSelection();
                        this.shownstep1 = true;
                        data.open();
                        this.ProcessingHandler(data);
                        // this.shownstep2 = true;
                        // this.step2class = this.activeclass;
                    }
                }, 2000);
            }
            else {
                this.ToastrService.error("Please accept terms and condition");
            }

        }
    }

    OTPConfirm() {
        if (this.panidentity.otp.length < 6) {
            setTimeout(() => {
                this.ToastrService.error("Please enter otp of 6 digits");
                return;
            }, 2000);
        }
        else {
            this.PanDetailSubmit(null, null);
        }
    }

    Step3Submit(f: NgForm) {
        if (f.valid) {
            //if (this.isAgreementChecked) {
            this.isRequestLoading = true;
            this.shownstep3 = false;
            this.showStepWizard = false;
            this.dynamicText = "you are being redirected to bank site for setting for verifying bank account.";
            setTimeout(() => {
                //this.isShowverification = true;
                //this.ClearSelection();
                //this.userservice.SaveUserwalletInfo("", "")
                //this.myRoute.navigate(['/mywallet'])
                this.dynamicText = "Your account verification has been successfully setup."
                setTimeout(() => {
                    this.isRequestLoading = false;
                    //this.isShowverification = true;
                    this.showTermsConditions = true;
                    //this.ClearSelection();
                    //this.userservice.SaveUserwalletInfo("", "")
                    //this.myRoute.navigate(['/mywallet'])
                }, 3000);
            }, 3000);
        }
    }
    DebitCardCheck(event) {
        if (event.target.checked) {
            this.showbankdetail = true;
            this.paymentMode = "DebitCard";
        }
        else {
            this.showbankdetail = false;
        }
    }
    eNACHCheckedUsingNetBanking(event) {
        if (event.target.checked) {
            this.showbankdetail = true;
            this.paymentMode = "eNACHCheckedUsingNetBanking";
        }
        else {
            this.showbankdetail = false;
        }
    }
    eNACHChecked(event) {
        if (event.target.checked) {
            this.showStepWizard = false;
            this.shownstep3 = false;
            this.showENACHForm = true;
        }
        else {
            this.showbankdetail = false;
        }
    }
    submitDebitagreementdetail(f: NgForm) {
        debugger;
        if (f.valid) {
            if (this.DebitaggrementdetailModel.isTermsconditionchecked) {
                this.isLoading = true;

                setTimeout(() => {
                    this.isLoading = false;
                    this.showTermsConditions = false;
                    this.showEsign = true;
                }, 2000)
            }
        }

    }


    openIdentityDetails(data: any) {
        data.open();
        //this.ProcessingHandler();

    }
    TermConditionChecked(event) {
        debugger
        if (event.target.checked)

            this.isAgreementChecked = true;
        else
            this.isAgreementChecked = false;
    }

    AcceptOfferCheckEvent(event) {
        debugger
        if (event.target.checked)

            this.isOfferChecked = true;
        else
            this.isOfferChecked = false;

    }
    Step2Submit() {
        if (this.isOfferChecked) {
            this.isLoading = true;
            setTimeout(() => {
                this.isLoading = false;
                this.ClearSelection();
                this.isloadingredirect = true;
                // this.shownstep3 = true;
                // this.showbankdetail = true;
                // this.ShowDebitCardDetail = false;
                // this.step3class = this.activeclass;
                this.userservice.SaveUserwalletInfo("", '90000')
                this.myRoute.navigate(['/mywallet'])
            }, 2000);
        }
        else {
            this.ToastrService.error("Please accept terms and condition for the Loan offer");
        }
    }

    ClearSelection() {
        this.shownstep1 = false;
        this.shownstep2 = false;
        this.shownstep3 = false;
        this.step1class = this.inactiveclass;
        this.step2class = this.inactiveclass;
        this.step3class = this.inactiveclass;
    }
    ShowDebitCardSection(event) {
        if (event.target.value) {
            this.showbankdetail = false;
            this.ShowDebitCardDetail = true;
        }
    }
    BankDetailsChanged(event) {
        debugger;
        if (event.target.value) {
            if (this.paymentMode == "DebitCard") {
                this.showbankdetail = false;
                this.ShowDebitCardDetail = true;
            }
            else if (this.paymentMode == "eNACHCheckedUsingNetBanking") {
                this.isRequestLoading = true;
                this.dynamicText = "You are being redirected to bank site for verifying bank account.";
                setTimeout(() => {
                    this.isRequestLoading = false;
                    this.myRoute.navigate(['/loginMerchant']);
                }, 2000
                );
            }
        }
    }
    ResendOTP() {
        this.isLoading = true;
        setTimeout(() => {
            this.isLoading = false;
            this.panidentity.otp = '';
            this.ToastrService.success("Otp send successfully on your registered aadhar mobile number.");
        }, 2000);
    }
    ProcessingHandler(data: any) {
        this.islevel1notprocessing = true;
        this.islevel2notprocessing = true;
        this.islevel3notprocessing = true;
        setTimeout(() => {
            this.firstProcess(data);

        }, 1000)
    }
    firstProcess(data: any) {
        this.islevel1notprocessing = false;
        this.islevel1processing = true;
        setTimeout(() => {
            this.islevel1processing = false;
            this.islevel1processed = true;
            this.ThirdProcess(data);
        }, 2000)
    }
    // SecondProcess(data: any) {
    //     this.islevel2notprocessing = false;
    //     this.islevel2processing = true;
    //     setTimeout(() => {
    //         this.islevel2processing = false;
    //         this.islevel2processed = true;
    //         this.ThirdProcess(data)
    //     }, 3000)
    // }

    ThirdProcess(data: any) {
        this.islevel3notprocessing = false;
        this.islevel3processing = true;
        setTimeout(() => {
            this.islevel3processing = false;

            this.ResetVerificationPopup(data);

        }, 2000)
    }
    ResetVerificationPopup(data) {
        this.islevel3processed = true;
        setTimeout(() => {

            data.close();
            this.islevel1notprocessing = false
            this.islevel1processing = false
            this.islevel1processed = false
            this.islevel2notprocessing = false
            this.islevel2processing = false
            this.islevel2processed = false
            this.islevel3notprocessing = false
            this.islevel3processing = false
            this.islevel3processed = false
            //proceed to second screen
            this.ClearSelection();
            this.shownstep2 = true;
            this.step2class = this.activeclass;
        }, 1000)
    }

    // Pie

    //   public randomizeType():void {
    //     this.pieChartType = this.pieChartType === 'doughnut' ? 'pie' : 'doughnut';
    //   }

    public chartClicked(e: any): void {
        console.log(e);
    }

    public chartHovered(e: any): void {
        console.log(e);
    }

    shownStep3(data) {
        debugger;
        if (this.isOfferChecked == true) {
            this.isLoading = true;
            setTimeout(() => {
                this.isLoading = false;
                this.shownstep2 = false;
                this.shownstep3 = true;
            }, 2000)
        }
        else {
            this.ToastrService.error("Please accept terms and condition");
        }
    }

    GetOTPVerification() {
        this.isLoading = true;
        setTimeout(() => {
            this.isLoading = false;
            this.showEsign = false;
            this.showOTPVerification = true;
        }, 2000)
    }

    ShowConfirmation() {
        this.isLoading = true;
        setTimeout(() => {
            this.isLoading = false;
            this.showOTPVerification = false;
            this.showConfirmation = true;
        }, 2000)
    }

    RedirectToLoading() {
        this.isRequestLoading = true;
        this.dynamicText = "Please wait while cart payment is being processed"
        this.showConfirmation = false;
        setTimeout(() => {
            this.isRequestLoading = false;
            this.myRoute.navigate(['/mywallet']);
        }, 2000)
    }

    AuthorizeeNACHForm() {
        this.isRequestLoading = true;
        this.dynamicText = "Your account verfication has been successfully setup.";
        setTimeout(() => {
            this.dynamicText = "Please wait while the cart payment is being precessed.";
            setTimeout(() => {
                this.isRequestLoading = false;
                this.myRoute.navigate(['/mywallet']);
            }, 3000
            );
        }, 3000
        );
    }
    CalculateEMI() {
        debugger;
        if (!isNullOrUndefined(this.EmiModel.CardAmount) && this.EmiModel.InterestRate != null && !isNullOrUndefined(this.EmiModel.Timeduration)) {


            let P: number = Number(this.EmiModel.CardAmount);
            let R: number = Number(this.EmiModel.InterestRate);
            let N: number = Number(this.EmiModel.Timeduration);
            let monthlyRate: number = R / (12 * 100);

            let powerCalculation = Math.pow((1 + monthlyRate), N);
            let FinalEMI = Math.round((P * monthlyRate * powerCalculation) / (powerCalculation - 1));

            this.EmiModel.EMIDetail = String(FinalEMI);
        }
        else {
            this.EmiModel.EMIDetail = "0";

        }
    }
}
