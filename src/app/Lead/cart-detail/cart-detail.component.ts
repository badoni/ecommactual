import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart-detail',
  templateUrl: './cart-detail.component.html',
  styleUrls: ['./cart-detail.component.css']
})
export class CartDetailComponent implements OnInit {

  constructor(private route:Router) { }

  ngOnInit() {
  }
  ProceedCheckout()
  {
    this.route.navigate(['/paymentmethod'])
  }
}
