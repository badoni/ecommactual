import { Component, OnInit, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BuiltinFunctionCall } from '@angular/compiler/src/compiler_util/expression_converter';
import { Router } from '@angular/router';
import { UserService } from '../../Services/User.Services';
import { Modal, ModalModule } from 'ngx-modal';
import jsQR from "jsqr";
import { TNodeFlags } from '@angular/core/src/render3/interfaces/node';
@Component({
    selector: 'app-eligibility',
    templateUrl: './eligibility.component.html',
    styleUrls: ['./eligibility.component.css'],
    providers: [UserService]
})
export class EligibilityComponent implements OnInit {
    @ViewChild('verifyDetail') someInput: ModalModule;
    panidentity = {
        'FirstName': '',
        'LastName': '',
        'Mobile': '',
        'Email': '',
        'PanNumber': '',
        'AadharNumber': '',
        'DOB': '',
        'Gender': '',
        'MarritalStatus': '',
        'Address1': '',
        'Address2': '',
        'Landmark': '',
        'State': '',
        'City': '',
        'PinCode': '',
        'FathersFirstName': '',
        'FathersLastName': '',
        'MothersFirstName': '',
        'MothersLastName': '',
        'SpousesFirstName': '',
        'SpousesLastName': '',
        'otp': '',
    }

    DebitdetailModel = {
        'DebitCardNumber': '',
        'DebitcardHolderName': '',
        'ExpiryMonth': '',
        'Expiryyear': '',
        'CVV': ''
    }

    video: any;
    localStream: any;
    islevel1notprocessing = false
    islevel1processing = false
    islevel1processed = false
    islevel2notprocessing = false
    islevel2processing = false
    islevel2processed = false
    islevel3notprocessing = false
    islevel3processing = false
    islevel3processed = false
    canvasElement;
    canvas;
    loadingMessage;
    outputContainer;
    outputMessage;
    outputData;
    Timeline: any;
    Qrdata: any;

    showProcessing: boolean = false;
    isShowverification: boolean = false;
    isLoading: boolean;
    shownstep2: boolean = false
    shownstep3: boolean = false
    shownstep1: boolean = false
    step1class = ''
    step2class = '';
    step3class = '';
    isAgreementChecked: boolean;
    inactiveclass: string = 'btn btn-circle btn-default';
    activeclass: string = 'btn btn-circle btn-default btn-primary';
    isOfferChecked: boolean = false;
    ShowDebitCardDetail: boolean = false;
    isloadingredirect: boolean = false;
    showbankdetail: boolean;
    showPaymentoption: boolean;
    showQr: boolean = false;
    timer: number = 0;
    istimeout: boolean = false;
    ScanAadharcode: boolean;
    constructor(private ToastrService: ToastrService, private myRoute: Router, private userservice: UserService) { }

    ngOnInit() {


        this.shownstep1 = true;
        this.step1class = this.activeclass;
        this.step2class = this.inactiveclass;
        this.step3class = this.inactiveclass;
        this.video = document.createElement("video");
        //document.body.appendChild(this.video);
        this.video.autoplay = true;

        this.canvasElement = document.getElementById("canvas");
        this.canvas = this.canvasElement.getContext("2d");
        this.loadingMessage = document.getElementById("loadingMessage");
        this.outputContainer = document.getElementById("output");
        this.outputMessage = document.getElementById("outputMessage");
        this.outputData = document.getElementById("outputData");

    }
    ngAfterViewInit() {

    }

    PanDetailSubmit(f: NgForm, data: any) {
        debugger;
        if (f.valid) {
            if (this.isAgreementChecked) {
                if (this.isShowverification && !this.panidentity.otp) {
                    if (this.panidentity.otp.length < 6) {
                        this.ToastrService.error("Please enter otp of 6 digits");
                        return;
                    }
                }
                this.isLoading = true;
                setTimeout(() => {
                    this.isLoading = false;
                    this.isShowverification = true;
                    if (this.panidentity.otp) {
                        this.ClearSelection();
                        this.shownstep1 = true;
                        data.open();
                        this.ProcessingHandler(data);
                        // this.shownstep2 = true;
                        // this.step2class = this.activeclass;
                    }
                }, 2000);
            }
            else {
                this.ToastrService.error("Please accept terms and condition");
            }
        }
    }
    Step3Submit(f: NgForm) {
        if (f.valid) {
            if (this.isAgreementChecked) {
                this.isloadingredirect = true;
                setTimeout(() => {
                    this.isloadingredirect = false;
                    this.isShowverification = true;

                    this.ClearSelection();
                    this.userservice.SaveUserwalletInfo(100000, 0)


                    this.myRoute.navigate(['/mywallet'])
                }, 3000);
            }
        }
    }
    Cancel() {
        this.myRoute.navigate(['/mywallet'])
    }
    DebitCardCheck(event) {
        if (event.target.checked) {
            this.showbankdetail = true;
        }
        else {
            this.showbankdetail = false;
        }
    }
    eNACHChecked() {
        this.ShowDebitCardDetail = false;
    }


    openIdentityDetails(data: any) {
        // data.open();
        //this.ProcessingHandler();

    }
    TermConditionChecked(event) {
        debugger
        if (event.target.checked)

            this.isAgreementChecked = true;
        else
            this.isAgreementChecked = false;
    }

    AcceptOfferCheckEvent(event) {
        debugger
        if (event.target.checked)

            this.isOfferChecked = true;
        else
            this.isOfferChecked = false;

    }
    Step2Submit() {
        // if (this.isOfferChecked) 

        this.isLoading = true;
        setTimeout(() => {
            this.isLoading = false;
            this.ClearSelection();

            this.userservice.SaveUserwalletInfo(100000, 0)


            this.myRoute.navigate(['/mywallet'])

            //this.shownstep3 = true;
            //this.showPaymentoption = true;
            // this.ShowDebitCardDetail = false;
            // this.step3class = this.activeclass;

        }, 2000);

        // else {
        //     this.ToastrService.error("Please accept terms and condition for the Loan offer");
        // }
    }
    ClearSelection() {
        this.shownstep1 = false;
        this.shownstep2 = false;
        this.shownstep3 = false;
        this.step1class = this.inactiveclass;
        this.step2class = this.inactiveclass;
        this.step3class = this.inactiveclass;
    }
    ShowDebitCardSection(event) {
        if (event.target.value) {
            this.showbankdetail = false
            this.ShowDebitCardDetail = true;
        }
    }
    ResendOTP() {
        this.isLoading = true;
        setTimeout(() => {
            this.isLoading = false;
            this.panidentity.otp = '';
            this.ToastrService.success("Otp send successfully on your registered aadhar mobile number.");
        }, 2000);
    }
    ProcessingHandler(data: any) {
        this.islevel1notprocessing = true;
        this.islevel2notprocessing = true;
        this.islevel3notprocessing = true;
        setTimeout(() => {
            this.firstProcess(data);
        }, 1000)
    }
    firstProcess(data: any) {
        this.islevel1notprocessing = false;
        this.islevel1processing = true;
        setTimeout(() => {
            this.islevel1processing = false;
            this.islevel1processed = true;
            this.ThirdProcess(data);
        }, 2000)
    }
    // SecondProcess(data: any) {
    //     this.islevel2notprocessing = false;
    //     this.islevel2processing = true;
    //     setTimeout(() => {
    //         this.islevel2processing = false;
    //         this.islevel2processed = true;
    //         this.ThirdProcess(data)
    //     }, 2000)
    // }

    ThirdProcess(data: any) {
        this.islevel3notprocessing = false;
        this.islevel3processing = true;
        setTimeout(() => {
            this.islevel3processing = false;
            this.ResetVerificationPopup(data);
        }, 3000)
    }

    ResetVerificationPopup(data) {
        this.islevel3processed = true;
        setTimeout(() => {

            data.close();
            this.islevel1notprocessing = false
            this.islevel1processing = false
            this.islevel1processed = false
            this.islevel2notprocessing = false
            this.islevel2processing = false
            this.islevel2processed = false
            this.islevel3notprocessing = false
            this.islevel3processing = false
            this.islevel3processed = false
            //proceed to second screen
            this.ClearSelection();
            this.shownstep2 = true;
            this.step2class = this.activeclass;
        }, 1000)
    }

    start() {
        if (window.innerWidth > 800) {
            this.Qrdata = '';
            this.showQr = true;
            this.initCamera({video: { 
                facingMode: "user",
                frameRate: { ideal: 10, max: 15 }   
            }, audio: false });
        }
        else {
            this.Qrdata = '';
            this.showQr = true;
            this.initCamera({video: { 
                facingMode: { exact: "environment"},
                frameRate: { ideal: 10, max: 15 } 
            }, audio: false });
        }
    }
    sound() {
        this.initCamera({ video: true, audio: true });
    }

    vidOff() {
        //this.video.pause();
        //this.video.src = "";
        // this.localstream.stop();
        //this.localStream.stop();
        //this.video.localstream.getVideoTracks().forEach(track => track.stop());
        console.log("camera off");
    }
    initCamera(config: any) {
        var browser = <any>navigator;
        // navigator.mediaDevices.getUserMedia
        // browser.getUserMedia = (browser.getUserMedia ||
        //   browser.webkitGetUserMedia ||browser.window.webkitURL||
        //   browser.mozGetUserMedia ||
        //   browser.msGetUserMedia);

        browser.mediaDevices.getUserMedia(config).then(stream => {
            debugger;
            this.localStream = stream;
            this.video.src = window.URL.createObjectURL(stream);
            this.video.play();
            requestAnimationFrame(this.tick1.bind(this));
        });
    }
    // Changes XML to JSON
    tick1() {
        let t = this;
        //  debugger;
        t.loadingMessage.innerText = "⌛ Loading video..."
        if (t.video.readyState === t.video.HAVE_ENOUGH_DATA) {
            t.loadingMessage.hidden = true;
            t.canvasElement.hidden = false;
            t.outputContainer.hidden = false;

            t.canvasElement.height = t.video.videoHeight;
            t.canvasElement.width = t.video.videoWidth;
            t.canvas.drawImage(t.video, 0, 0, t.canvasElement.width, t.canvasElement.height);
            var imageData = t.canvas.getImageData(0, 0, t.canvasElement.width, t.canvasElement.height);
            var code = jsQR(imageData.data, imageData.width, imageData.height)
            //   , {
            //    inversionAttempts: "dontInvert",
            //  });
            if (code) {
                t.drawLine(code.location.topLeftCorner, code.location.topRightCorner, "#FF3B58");
                t.drawLine(code.location.topRightCorner, code.location.bottomRightCorner, "#FF3B58");
                t.drawLine(code.location.bottomRightCorner, code.location.bottomLeftCorner, "#FF3B58");
                t.drawLine(code.location.bottomLeftCorner, code.location.topLeftCorner, "#FF3B58");
                t.outputMessage.hidden = true;
                t.outputData.parentElement.hidden = false;
                t.outputData.innerText = code.data;
                if (!t.Qrdata) {
                    debugger;
                    t.Qrdata = code.data
                    if (!code.data) {
                        this.istimeout = true;
                        this.showQr = false;
                        //this.vidOff();
                        return;
                    }
                    console.log(t.Qrdata);
                    var text, parser, xmlDoc;
                    parser = new DOMParser();
                    xmlDoc = parser.parseFromString(t.Qrdata, "text/xml");
                    let attributes = xmlDoc.children[0].attributes;
                    let attibutArray = [];
                    for (let i = 0; i < attributes.length; i++) {
                        attibutArray.push(attributes[i].name);
                    }
                    var _dateofbirth = '';
                    // var _dateofbirth = xmlDoc.children[0].attributes.dob.nodeValue;
                    var _name = xmlDoc.children[0].attributes.name.nodeValue;
                    var _Gender = xmlDoc.children[0].attributes.gender.nodeValue;
                    var _AadharNumber = xmlDoc.children[0].attributes.uid.nodeValue;
                    this.panidentity.AadharNumber = _AadharNumber;
                    this.showQr = true;
                    this.ScanAadharcode = true;
                    //this.vidOff()
                }
            } else {
                t.outputMessage.hidden = false;
                t.outputData.parentElement.hidden = true;
            }
        }
        requestAnimationFrame(this.tick1.bind(this));
    }
    CancelQrmodal() {
        this.showQr = false;
    }
    drawLine(begin, end, color) {
        this.canvas.beginPath();
        this.canvas.moveTo(begin.x, begin.y);
        this.canvas.lineTo(end.x, end.y);
        this.canvas.lineWidth = 4;
        this.canvas.strokeStyle = color;
        this.canvas.stroke();
    }
    CloseQrScanning() {
        this.showQr = false;
        this.ScanAadharcode = true;

    }

    IsSpouseEnable: boolean = false;
    MarritalStatusChange(status) {
        debugger;
        if (status == 'Single') {
            this.IsSpouseEnable = false;
        }
        else {
            this.IsSpouseEnable = true;
        }
    }
}
