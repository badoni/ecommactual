import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-paymentmethod',
  templateUrl: './paymentmethod.component.html',
  styleUrls: ['./paymentmethod.component.css']
})
export class PaymentmethodComponent implements OnInit {
  selectedPaymentoption: string='';

  constructor(private route:Router) { }

  ngOnInit() {
  }
  Selectpaymentoption(event: any) {
    if (event.target.checked) {
     // alert(event.target.value)
      this.selectedPaymentoption = event.target.value;
      
    }
  }
  ProceedToPayment()
  {
    if(this.selectedPaymentoption.toLowerCase()=='newway')
      {
        this.route.navigate(['/eligibilityDetail'])
      }
  }
}
