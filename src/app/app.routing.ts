import { Routes, RouterModule } from '@angular/router';
import { SiteLayoutComponent } from './Layout/site-layout/site-layout.component';
import { AppLayoutComponent } from './Layout/app-layout/app-layout.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './Auth/login/login.component';
import { MerchantloginComponent } from './Auth/merchantlogin/merchantlogin.component';
import { MyWalletComponent } from './my-wallet/my-wallet.component';
import { LogoutComponent } from './logout/logout.component';
import { EligibilityComponent } from './Lead/eligibility/eligibility.component';
import { ProductDetailComponent } from './Lead/product-detail/product-detail.component';
import { CartDetailComponent } from './Lead/cart-detail/cart-detail.component';
import { PaymentmethodComponent } from './Lead/paymentmethod/paymentmethod.component';
import { BonuspageComponent } from './Lead/bonuspage/bonuspage.component';
import { MarchantloginsuccessComponent } from "./Auth/marchantloginsuccess/marchantloginsuccess.component";


const appRoutes: Routes = [
    
    { path: '', component: LoginComponent},
    //Site routes goes here 
    { 
        path: '', 
        component: SiteLayoutComponent,
        children: [
            { path: 'dashboard', component: DashboardComponent},
            { path: 'mywallet', component: MyWalletComponent},
            
            { path: 'productDetail', component: ProductDetailComponent},
            {path:'CartDetail',component:CartDetailComponent},
            {path:'paymentmethod',component:PaymentmethodComponent},
           
        //   { path: '', component: HomeComponent, pathMatch: 'full'},
        //   { path: 'about', component: AboutComponent },
        //   { path: 'test/:id', component: AboutComponent }
        ]
    },
    
    // App routes goes here here
    { 
        path: '',
        component: AppLayoutComponent, 
        children: [
            {path:'eligibilityDetail',component:BonuspageComponent},
            { path: 'eligibility', component: EligibilityComponent},
        //  { path: 'profile', component: ProfileComponent }
         ]
    },

    //no layout routes
    { path: 'login', component: LoginComponent},
    { path: 'loginMerchant', component: MerchantloginComponent},
    { path: 'logout', component: LogoutComponent},
    { path: 'marchantloginsuccess', component: MarchantloginsuccessComponent},
    { path: '**', redirectTo: '/login' }
];

export const routing = RouterModule.forRoot(appRoutes);


