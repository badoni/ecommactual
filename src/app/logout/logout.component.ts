import { Component, OnInit } from '@angular/core';
import { UserService } from '../Services/User.Services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
  providers: [UserService]
})
export class LogoutComponent implements OnInit {

  constructor(private userService: UserService, private route: Router) { }

  ngOnInit() {
    this.SetLogOut();
  }
  SetLogOut() {
    this.userService.logout();
    this.route.navigate(['/login'])

  }

}
