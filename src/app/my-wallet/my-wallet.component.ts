import { Component, OnInit } from '@angular/core';
import { UserService } from '../Services/User.Services';

@Component({
  selector: 'app-my-wallet',
  templateUrl: './my-wallet.component.html',
  styleUrls: ['./my-wallet.component.css'],
  providers: [UserService]
})
export class MyWalletComponent implements OnInit {
  iseligibilityCompleted: boolean = false;
  walletamount: any;
  constructor(private userService: UserService) { }

  ngOnInit() {
    debugger;
    var currentuser = this.userService.GetCurrentUser();
    if (currentuser.length > 0) {
      var s = localStorage.getItem("currentUser")
      if (s.length > 0) {
        var userinfo = JSON.parse(s)
        if (userinfo[0]['EligibleAmount'] != '0') {
          this.iseligibilityCompleted = true;
          this.walletamount=userinfo[0]['EligibleAmount'];
        }
        else {
          this.iseligibilityCompleted = false;
          this.walletamount=userinfo[0]['EligibleAmount'];
        }
      }
    }
    else {
      this.iseligibilityCompleted = false;
    }
  }
  
}
